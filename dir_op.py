import os

def getfiles(path, list1):  # 递归法获取文件夹及子文件夹内所有文件
	dirs = os.listdir(path)  # 获取当前文件夹路径下的所有文件或文件夹列表
	for i in dirs:
		k = os.path.join(path, i)
		if os.path.isdir(k):
			getfiles(k, list1)
		else:
			list1.append(k)


def getdirs(path, list2):  # 递归法获取文件夹及子文件夹内所有文件夹
	dirs = os.listdir(path)
	for i in dirs:
		k = os.path.join(path, i)
		if os.path.isdir(k):
			list2.append(k)
			getdirs(k, list2)
	return list2
