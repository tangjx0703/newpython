#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 20:14:12 2021

@author: tangjx
"""

import pandas as pd
import numpy as np
import re
import os
import matplotlib.pyplot as plt
#解决中文显示问题
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False
"""
获取巡检任务信息
"""
def get_task(file_path):
    temp1 = os.path.basename(file_path)
    ###Exp保存各类正则表达式，获取巡检点位数，环境数据，时间，巡检次数，编号，可补充添加
    Condition = ['点位数','环境','时间','巡检次数','编号','变电站']
    Exp={}
    Exp[Condition[0]] = r'本次任务共巡视(\d+)点位, 正常点位:(\d+)个, 告警点位:(\d+)个, 识别异常点位:(\d+),漏检点位数:(\d+)'
    Exp[Condition[1]] = r'环境温度:(.+)、环境湿度:(.+)、环境风速:(.+)、风向:(.+)'
    Exp[Condition[2]] = r'开始时间:(.+); 结束时间:(.+); 总时长'
    Exp[Condition[3]] = r'(\d{14})'
    Exp[Condition[4]] = r'(\w\w变)'
    ### temp，i临时变量，i记录当前任务数条目，temp表示包含17列数据
    temp = [np.nan for x in range(1,18)]
    result=[]
    df1 = pd.read_excel(file_path)
    temp[1] = df1.columns[1]
    temp[3] = df1.iloc[1,1]
    temp[6] = df1.iloc[2,1]
    temp[12] = df1.iloc[3,1]
    result.append(re.compile(Exp[Condition[0]]).findall(temp[6])) 
    result.append(re.compile(Exp[Condition[1]]).findall(temp[12])) 
    result.append(re.compile(Exp[Condition[2]]).findall(df1.iloc[0,1]))
    for k in range(0,5):
        temp[7+k] = result[0][0][k]
    for k in range(0,4):
        temp[13+k] = result[1][0][k]
    for k in range(0,2):
        temp[4+k] = result[2][0][k]
    pan4 = re.compile(Exp[Condition[4]])
    result4 = pan4.match(temp1)
    pan6 = re.compile(Exp[Condition[3]])
    result6 = pan6.findall(df1.columns[1])
    temp[2] = result4.group()
    temp[0] = result4.group()+str(result6[0]) 
    return temp
# file_path ='110表计_20201125070000_20201202070005.xls'
# colname = ['任务编号','任务名称','变电站','任务状态','开始时间','结束时间','巡检点','总点位','正常点位','告警点位','识别异常点位','漏检点位','环境信息','环境温度','环境湿度','环境风速','风向']
# df2 = pd.DataFrame(columns = colname)
# df2.loc[0] = get_task(file_path)
# print(df2)
# """
# 获取巡检任务结果信息
# """

def get_result(file_path,df,xh):
    # colname1 = ['序号','点位名称','识别类型','识别时间','环境温度','识别结果','采集信息1','采集信息2','告警等级','结果分类','变电站','任务编号']
    # df3 = pd.DataFrame(columns = colname1)
    temp1 = os.path.basename(file_path)
    r1 = r'(\d{14})'
    r2 = r'(\w\w变)'
    df1 = pd.read_excel(file_path)
    Points = ['告警点位','识别异常点位','正常点位']
    Pposition = {}
    for i in range(0,df1.shape[0]):
        for tempstr in Points:
            if df1.iloc[i,0] == tempstr:
                Pposition[tempstr] = i
    pan4 = re.compile(r2)
    result4 = pan4.match(temp1)
    pan6 = re.compile(r1)
    result6 = pan6.findall(df1.columns[1])
    
    # print(Pposition)
    if (Pposition['识别异常点位']>Pposition['告警点位']+1):
        for i in range(Pposition['告警点位']+2,Pposition['识别异常点位']):
            temp1 = [np.nan for x in range(0,12)]
            temp1[0]= xh
            for j in range(1,9):
                temp1[j]= df1.iloc[i,j]
            temp1[10]= result4.group()
            temp1[11] = result4.group()+str(result6[0])
            temp1[9] = '告警'
            df.loc[xh] =temp1
            xh = xh+1
    if (Pposition['正常点位']>Pposition['识别异常点位']+1):
        for i in range(Pposition['识别异常点位']+2,Pposition['正常点位']):
            temp1 = [np.nan for x in range(0,12)]
            temp1[0]= xh
            for j in range(1,9):
                temp1[j]= df1.iloc[i,j]
            temp1[10]= result4.group()
            temp1[11] = result4.group()+str(result6[0])
            temp1[9] = '识别异常'
            df.loc[xh] =temp1
            xh = xh+1
    if (df1.shape[0]>Pposition['正常点位']+1):
        for i in range(Pposition['正常点位']+2,df1.shape[0]):
            temp1 = [np.nan for x in range(0,12)]
            temp1[0]= xh
            for j in range(1,9):
                temp1[j]= df1.iloc[i,j]
            temp1[10]= result4.group()
            temp1[11] = result4.group()+str(result6[0])
            temp1[9] = '正常'
            df.loc[xh] =temp1
            xh = xh+1
    return xh


def get_default_point(file_path, station):
    df = pd.read_excel(file_path)
    df['Station'] = station
    df_columns = ['DeviceName','PatrolType','DeviceType','DeviceRegion','LineName','IntervalName','Station']
    return df[df_columns]
