import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
# import threading.Thread as Thread
# import processing.Process as Process
import os
import sys
import data_input
import sql_op
import dir_op
import I_P
import default_point_ansys

if __name__ == '__main__':
    Tuser, Tpassword, Thost, Tdatabase, Tport = I_P.dbinfo()
    # mysqlio = sql_op.mysqlIO(Tuser, Thost, Tport, Tpassword)
    # mysqlio.dbconnect(Tdatabase)
    pdsqlio = sql_op.pd_sql(Tuser, Thost, Tport, Tpassword, Tdatabase)
    ds_p_ansys = default_point_ansys.de_sys()
    dbtable = ['inspe_result','inspe_task','default_point']
    selected = """----------------------------------
    1、生成新的巡检任务清单并导入数据库
    2、生成新的巡检结果清单并导入数据库
    3、初始化各站预设点位
    4、各站预设点位分析
    0、退出程序
    """
    print(selected)
    while (True):
        flag = int(input('请输入需要操作的序号：'))
        if (flag < 1 and flag > 2):
            print('输入错误！')
            flag = int(input('请输入需要操作的序号：'))
        else:
            if flag == 1:
                j = 0
                colname = ['任务编号', '任务名称', '变电站', '任务状态', '开始时间', '结束时间', '巡检点', '总点位',
                           '正常点位', '告警点位', '识别异常点位', '漏检点位', '环境信息', '环境温度', '环境湿度', '环境风速', '风向']
                df = pd.DataFrame(columns=colname)
                filepath = input('请输入巡检数据记录表所在文件夹：')
                filelist = []
                dir_op.getfiles(filepath, filelist)
                for i in filelist:
                    df.loc[j] = data_input.get_task(i)
                    j = j + 1
                print(df.info())
                pdsqlio.save_data(dbtable[1],df)
                # df.to_excel('芦江变202012巡检任务.xlsx')
                # df = pd.DataFrame
                # outputfiles(filepath)
            elif flag == 2:
                colname1 = ['序号', '点位名称', '识别类型', '识别时间', '环境温度',
                            '识别结果', '采集信息1', '采集信息2', '告警等级', '结果分类', '变电站', '任务编号']
                df = pd.DataFrame(columns=colname1)
                xh = df.shape[0]
                filepath = input('请输入巡检数据记录表所在文件夹：')
                filelist = []
                dir_op.getfiles(filepath, filelist)
                for i in filelist:
                    xh = data_input.get_result(i, df, xh)
                    # print(xh)
                print(df.info())
                pdsqlio.save_data(dbtable[0],df)
                
            elif flag == 3:
                
                
                filepath = input('请输入巡检数据记录表所在文件夹：')
                filelist = []
                dir_op.getfiles(filepath, filelist)
                for i in filelist:
                    df = pd.DataFrame()
                    temp = os.path.basename(i)
                    df = data_input.get_default_point(i, temp.split('.')[0])
                #     # print(xh)
                # print(df.info())
                    pdsqlio.save_data(dbtable[2], df)
                print('各站机器人预设点位初始化成功！')
            elif flag == 4:
                # ds_p_ansys.station_count_ansys(pdsqlio)
                # ds_p_ansys.PatrolType_ansys(pdsqlio)
                # ds_p_ansys.Region_ansys(pdsqlio)
                ds_p_ansys.LineName_ansys(pdsqlio)

            elif flag == 0:
                print('结束！')
                break
            else:
                print('输入错误！请重新选择')
    
