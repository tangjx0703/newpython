import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
# import threading.Thread as Thread
# import processing.Process as Process
import os
import sys
import robot_function1


def getfiles(path, list1):  # 递归法获取文件夹及子文件夹内所有文件
	dirs = os.listdir(path)  # 获取当前文件夹路径下的所有文件或文件夹列表
	for i in dirs:
		k = os.path.join(path, i)
		if os.path.isdir(k):
			getfiles(k, list1)
		else:
			list1.append(k)


def getdirs(path, list2):  # 递归法获取文件夹及子文件夹内所有文件夹
	dirs = os.listdir(path)
	for i in dirs:
		k = os.path.join(path, i)
		if os.path.isdir(k):
			list2.append(k)
			getdirs(k, list2)
	return list2


if __name__ == '__main__':

    selected = """----------------------------------
    1、生成新的巡检任务清单
    2、生成新的巡检结果清单
    3、
    0、退出程序
    """
    print(selected)
    while (True):
        flag = int(input('请输入需要操作的序号：'))
        if (flag < 1 and flag > 2):
            print('输入错误！')
            flag = int(input('请输入需要操作的序号：'))
        else:
            if flag == 1:
                j = 0
                colname = ['任务编号', '任务名称', '变电站', '任务状态', '开始时间', '结束时间', '巡检点', '总点位',
                        '正常点位', '告警点位', '识别异常点位', '漏检点位', '环境信息', '环境温度', '环境湿度', '环境风速', '风向']
                df = pd.DataFrame(columns=colname)
                filepath = input('请输入巡检数据记录表所在文件夹：')
                filelist = []
                getfiles(filepath,filelist)
                for i in filelist:
                    df.loc[j] =robot_function1.get_task(i)
                    j = j +1
                print(df.info())
                df.to_excel('芦江变202012巡检任务.xlsx')
                # df = pd.DataFrame
                # outputfiles(filepath)
            elif flag ==2: 
                colname1 = ['序号', '点位名称', '识别类型', '识别时间', '环境温度',
                            '识别结果', '采集信息1', '采集信息2', '告警等级', '结果分类', '变电站', '任务编号']
                df = pd.DataFrame(columns = colname1)
                xh = df.shape[0]
                filepath = input('请输入巡检数据记录表所在文件夹：')
                filelist = []
                getfiles(filepath, filelist)
                for i in filelist:
                    xh = robot_function1.get_result(i, df, xh)
                    # print(xh)
                print(df.info())
                df.to_excel('芦江变202012巡检结果.xlsx')

            elif flag == 0:
                print('结束！')
                break
            else:
                print('输入错误！请重新选择')
