#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 18:43:26 2021

@author: tangjx
"""
import os
import re
import pandas as pd
import matplotlib.pyplot as plt
#解决中文显示问题
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False
"""
获取文件夹所有巡检数据文件，存放在excelarray数组中（excel格式记录）
"""
def get_files(filepath):    
    fileall= os.listdir(filepath)
    excel_array = []
    sta_array = []
    tr = re.compile(r'(.+)点位')
    for f in fileall:
        r = tr.findall(f)
        sta_array.append(r[0])
        k = os.path.join(filepath,f)
        if (os.path.isfile(k) and (('xls' in f) or ('xlsx' in f))):
            excel_array.append(k)
    df = pd.read_excel(excel_array[0])
    df['Station'] = sta_array[0]+'变'
    for i in range(1,len(sta_array)):
        df1 = pd.read_excel(excel_array[i])
        df1['Station'] = sta_array[i]+'变'
        df = pd.concat([df,df1],axis = 0)
    return df

def ansys(df,Station):
    temp = []
    temp.append('变电站')
    for k in df['PatrolType'].unique():
        temp.append(k)
    i = 0
    df1 = pd.DataFrame(columns = temp)
    for tempstr in Station:
        temp1 = []
        temp1.append(tempstr)
        t = df[df['Station'] == tempstr]['PatrolType'].value_counts()
        for u in temp[1:]:
            temp1.append(t[u])
        df1.loc[i] = temp1
        i = i+1
    df1['总点位'] = df1['红外测温（精测）'] + df1['设备外观检查（图片保存）']+df1['位置状态识别'] + df1['表计读取']
    return df1
    



"""
各站不同类型巡检点位占比分析
"""
def huatu(df):
    lieming = ['红外测温（精测）','设备外观检查（图片保存）','位置状态识别','表计读取']
    i = 0 
    plt.figure(figsize=(15,15))
#     fig,subs=plt.subplots(1,6)
    plt.subplots_adjust(hspace=0.3, wspace=0.3)
    for tempstr in df['变电站']:
        plt.subplot(2,3,i+1)
        plt.pie(x = df[lieming].iloc[i],labels = lieming,autopct='%.3f%%')
        temp = tempstr+'各类巡检点位占比'
        plt.title(temp)
        i=i+1
    plt.show
# huatu(df1)
"""
各站不同类型巡检点位占比分析
"""
def huatu2(df):
    plt.figure(figsize = (15,15))
    plt.subplots_adjust(hspace=0.3, wspace=0.3)
    plt.bar(x=df['变电站'],height = df['总点位'],width = 0.2 )
    plt.title('各站不同类型巡检点位比较')
    plt.show()


def search_dian(Station,jiange,leixing,filepath):
    df = pd.read_excel(filepath)
    # print(df[df['Station']==Station and (jiange in df['LineName']) and (df['PatrolType']==leixing)])
    print(df[df['Station']==Station])

def search_dian1(Station,leixing,jiange,filepath):
    df = pd.read_excel(filepath)
    d = df['LineName'].map(lambda x : jiange in x)
    # print(df[df['Station']==Station and (jiange in df['LineName']) and (df['PatrolType']==leixing)])
    print(df[(df['Station']==Station) & (df['PatrolType']==leixing) & d])
# if __name__=='__main__':