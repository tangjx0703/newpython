#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 19:11:20 2021

@author: tangjx
"""

import pandas as pd
import numpy as np
import re
import os
import matplotlib.pyplot as plt
#解决中文显示问题
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False

def ansys_renwu(df,Station):
    # print(df[df['Station']==Station]['PatrolType'].value_counts())
    # print(df[df['Station']==Station]['DeviceRegion'].value_counts())
    # print(df[df['Station']==Station]['DeviceType'].value_counts())
    # print(df[df['Station']==Station]['DeviceType'].value_counts())
    # print(df[df['Station']==Station][['PatrolType','DeviceRegion','DeviceName']].groupby(['PatrolType','DeviceRegion']).count())
    # print(df[df['Station']==Station][['PatrolType','DeviceRegion','LineName','DeviceName']].groupby(['PatrolType','DeviceRegion','LineName']).count())
    
    # print(pd.pivot_table(df[df['Station']==Station][['PatrolType','DeviceRegion','LineName','DeviceName']],index = ['PatrolType','DeviceRegion','LineName'],values= 'DeviceName',aggfunc = 'count'))
    # df1 = pd.pivot_table(df[df['Station']==Station][['PatrolType','DeviceRegion','LineName','DeviceName']],index = ['PatrolType','DeviceRegion','LineName'],values= 'DeviceName',aggfunc = 'count')
    # df1.to_excel('r.xlsx')
    df2 = df[df['Station']==Station][['PatrolType','DeviceRegion','LineName','DeviceName']].groupby(['PatrolType','DeviceRegion','LineName']).count()
    # df2.to_excel('r1.xlsx')
    print(type(df2))

    
path1 = '各站点位列表.xlsx'
Station = '江南变'
df = pd.read_excel(path1)
ansys_renwu(df,Station)
print(type(df))