import pandas as pd
import numpy as np
import re
import os
import matplotlib.pyplot as plt
import datetime
import time
#解决中文显示问题
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False

def add_z(x,y,z,w):
    return (x+y+z+w)

def pp(x):
    if(x['结果_正常']+x['结果_告警']+x['结果_识别异常'])>0:
        return ('%.3f'%(x['结果_正常']/(x['结果_正常']+x['结果_告警']+x['结果_识别异常'])))
    else:
        return np.nan
    
'''巡检季节'''
def riqi(x):
    structime = datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S")
    dt = structime.date().month
    se = [1,4,7,10,12]
    seasion = ''
    if dt>=se[0] and dt<se[1]:
        seasion = '第一季度'
    elif dt>=se[1] and dt<se[2]:
        seasion = '第二季度'
    elif dt>=se[2] and dt<se[3]:
        seasion = '第三季度'
    else:
        seasion = '第四季度'
    return seasion
'''巡检时间段'''
def shiduan(x):
    structime = datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S")
    ti = structime.time().hour
    sq = [0,6,12,18,24]
    sd =''
    if ti>=sq[0] and ti<sq[1]:
        sd = '0-6时'
    elif ti>=sq[1] and ti<sq[2]:
        sd = '6-12时'
    elif ti>=sq[2] and ti<sq[3]:
        sd = '12-18时'
    else:
        sd = '18-24时'
    return sd
'''时间格式转换'''
def type_trans(x):
    return datetime.datetime.strptime(x,"%Y-%m-%d %H:%M:%S")
'''巡检时间差'''
def dela(x,y):
    t_pre = datetime.datetime.strptime(x,"%Y-%m-%d %H:%M:%S")
    t_las = datetime.datetime.strptime(y,"%Y-%m-%d %H:%M:%S")
    # # print(t_las)
    t_dela = (t_las-t_pre).seconds
    return t_dela

def anays_pd(Start_time,Stop_time,df_exe,df_task,df_loc,flag):
    col= ['任务编号','识别类型','结果_正常','结果_告警','结果_识别异常','设备区域']
    col1 = ['任务编号','变电站','任务状态','开始时间','结束时间','总点位','漏检点位','识别类型','结果_正常','结果_告警','结果_识别异常','设备区域','环境温度','环境湿度','环境风速','风向']
    str1 = df_exe['识别时间'].map(lambda x:Start_time <= x <= Stop_time)
    df1 = pd.merge(df_exe[str1],df_loc,left_on = '点位名称',right_on = 'DeviceName',how = 'left')
    temp = [np.nan for x in range(1,7)]
    df = pd.DataFrame(columns=col)
    l =0
    for i in df1['任务编号'].unique():
        temp[0] = i
        temp[1] = df1[df1['任务编号']==i]['识别类型'].unique()
        temp[2] = df1[(df1['任务编号']==i)&(df1['结果分类']=='正常')].shape[0]
        temp[3] = df1[(df1['任务编号']==i)&(df1['结果分类']=='告警')].shape[0]
        temp[4] = df1[(df1['任务编号']==i)&(df1['结果分类']=='识别异常')].shape[0]
        temp[5] = df1[df1['任务编号']==i]['DeviceRegion'].unique()
        df.loc[l] =temp
        l =l+1
    # print(df)
    # print(df_task)
    df2 = pd.merge(df,df_task,on = '任务编号',how ='left')
    # print(df2.shape)
    df3 = df2[col1]
    # df3['总点位'] = df3[['结果_正常','结果_告警','结果_识别异常','漏检点位']].apply(add_z)
    # df3['巡检正常占比'] = df3[['结果_正常','结果_告警','结果_识别异常']].apply(pp)
    
    df3['巡检正常占比'] = df3['结果_正常']/(df3['结果_正常']+df3['结果_告警']+df3['结果_识别异常'])
    df3['总点位'] = df3['结果_正常']+df3['结果_告警']+df3['结果_识别异常']+df3['漏检点位']
    df3['巡检季节'] = df3['开始时间'].apply(riqi)
    df3['巡检时间段'] = df3['开始时间'].apply(shiduan)
    df3['巡检时间差'] = df3.apply(lambda x:dela(x['开始时间'],x['结束时间']),axis = 1)
    # df3['巡检时间差'] = df3.apply(lambda x:(datetime.datetime.strptime(x['结束时间'], "%Y-%m-%d %H:%M:%S")-datetime.datetime.strptime(x['开始时间'], "%Y-%m-%d %H:%M:%S")).seconds,axis=1)
    df3['平均巡检时间（s）'] = df3['巡检时间差']/(df3['结果_正常']+df3['结果_告警']+df3['结果_识别异常'])
    df3['巡检时间差'] = df3['巡检时间差'].apply(lambda x:(x/3600))
    df3['开始时间'] = df3['开始时间'].apply(type_trans)
    df3['结束时间'] = df3['结束时间'].apply(type_trans)
    return df3

path1 = '巡检结果记录表.xlsx'
path2 = '巡检任务记录表.xlsx'
path3 = '各站点位列表.xlsx'
df = pd.read_excel(path1)
df1 = pd.read_excel(path3)
df2 = pd.read_excel(path2)
t1 =  '2020-11-02 12:00:00'
t2 =  '2020-12-30 22:33:00'
ceshi = df['识别时间'].map(lambda x:t1<x<t2)
# df[(df['变电站']=='江南变')&ceshi]
# df[(df['变电站']=='江南变')]
# coverage('江南变',t1,t2,df,df1,True)
# df2=df['点位名称'].value_counts()
# get_result('江南变',t1,t2,df,True)
# df['识别类型'].unique()
# anays_detail('江南变',t1,t2,df[df['识别类型']=='表计读取'],'表计读取',False)
# print(df['任务编号'].value_counts())
# print(df.groupby('任务编号').sum())
df3 = anays_pd(t1,t2,df,df2,df1,True)

# print(df3.info())
# # i = datetime.datetime(df3['开始时间'].loc[0])

# print(df3[['开始时间','结束时间']])
print(df3)


