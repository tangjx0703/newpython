#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 22:00:59 2021

@author: tangjx
"""

import pandas as pd
import numpy as np
import re
import os
import matplotlib.pyplot as plt
#解决中文显示问题
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False

colname = ['任务编号','任务名称','变电站','任务状态','开始时间','结束时间','巡检点','总点位','正常点位','告警点位','识别异常点位','漏检点位','环境信息','环境温度','环境湿度','环境风速','风向']
colname1 = ['序号','点位名称','识别类型','识别时间','环境温度','识别结果','采集信息1','采集信息2','告警等级','结果分类','变电站','任务编号']


    
"""一定时间段某变电站巡检点位覆盖率"""
def coverage(Station,Start_time,Stop_time,df_exe,df_total,flag):
    str1 = df_exe['变电站']==Station
    str2 = df_exe['识别时间'].map(lambda x:Start_time <= x <= Stop_time)
    str3 = df_total['Station']==Station
    i = len(df_exe[str1 & str2]['点位名称'].unique())
    j = len(df_total[str3]['DeviceName'].unique())
    lv = i/j
    temp = '从 %s 至 %s 期间,巡检任务覆盖点位：%d，总点位：%d；巡检覆盖率：%f'%(Start_time,Stop_time,i,j,lv)
    plt.pie(x = [i,j-i],labels = ['已巡检点位数','未巡检点位数'])
    plt.title(Station+'巡检覆盖情况')
    print(temp)
    if flag == True:
        f = open('%s%s%s未巡检点位表.txt'%(Station,Start_time,Stop_time),'a')
        for tp in df_total[str3]['DeviceName'].unique():
            if tp in df_exe[str1 & str2]['点位名称'].unique():
                pass
            else:
                f.write(tp+'\n')
    else:
        pass
    
"""一定时间段某变电站巡检点位完全正确率、至少出现一次错误的比例"""
def anays_result(Station,Start_time,Stop_time,df_exe,flag):
    str1 = df_exe['变电站']==Station
    str2 = df_exe['识别时间'].map(lambda x:Start_time <= x <= Stop_time)
    str3 = df_exe['结果分类']=='告警'
    str4 = df_exe['结果分类']=='识别异常'
    str5 = df_exe['结果分类']=='正常'
    r = []
    r.append(df_exe[str1 & str2 & str3].shape[0])
    r.append(df_exe[str1 & str2 & str4].shape[0])
    r.append(df_exe[str1 & str2 & str5].shape[0])
    total = df_exe[str1 & str2].shape[0]
    temp = ('%s从 %s 至 %s 期间,巡检结果中正常点位：%d，占比：%.3f%%；'
           '告警点位：%d，占比：%.3f%%；识别异常点位：%d，'
            '占比：%.3f%%；'%(Station,Start_time,Stop_time,r[2],r[2]/total,r[1],r[1]/total,r[0],r[0]/total))
    plt.pie(x = [r[2]/total,r[1]/total,r[0]/total],labels = ['正常','识别异常','告警'])
    plt.title(Station+'巡检结果一览')
    print(temp)
    if flag == True:
        f1 = open('%s%s%s巡检识别异常点位（至少出现过一次）.txt'%(Station,Start_time,Stop_time),'a')
        f2 = open('%s%s%s巡检告警点位（至少出现过一次.txt'%(Station,Start_time,Stop_time),'a')
        for tp in df_exe[str1 & str2 & str4]['点位名称'].unique():
            f1.write(tp+'\n')
        for tp in df_exe[str1 & str2 & str3]['点位名称'].unique():
            f2.write(tp+'\n')
    else:
        pass
    """df1是识别异常数据，df2是告警数据，df_total是总的巡检数据"""
    dict1 = {'点位名称':df_exe[str1 & str2 & str4]['点位名称'].value_counts().index,'识别异常次数':df_exe[str1 & str2 & str4]['点位名称'].value_counts().values}
    df1 = pd.DataFrame(dict1)
    dict2 = {'点位名称':df_exe[str1 & str2 & str3]['点位名称'].value_counts().index,'告警次数':df_exe[str1 & str2 & str3]['点位名称'].value_counts().values}
    df2 = pd.DataFrame(dict2)
    
    dict_total = {'点位名称':df_exe[str1 & str2]['点位名称'].value_counts().index,'巡检总次数':df_exe[str1 & str2]['点位名称'].value_counts().values}
    df_total = pd.DataFrame(dict_total)
    
    df_abn = pd.merge(df1,df_total,on = '点位名称',how = 'left')
    df_alarm = pd.merge(df2,df_total,on = '点位名称',how = 'left')
    df_t1 = pd.merge(df_total,df1,on = '点位名称',how = 'left')
    df_t = pd.merge(df_t1,df2,on = '点位名称',how = 'left')
    file_name = []
    file_name.append('%s%s%s巡检识别异常点位总表.xlsx'%(Station,Start_time,Stop_time))
    file_name.append('%s%s%s巡检告警点位总表.xlsx'%(Station,Start_time,Stop_time))
    file_name.append('%s%s%s巡检结果总表.xlsx'%(Station,Start_time,Stop_time))
    df_abn.to_excel(file_name[0])
    df_alarm.to_excel(file_name[1])
    df_t.to_excel(file_name[2])
    
    
"""一定时间段某变电站不同类型的巡检点位告警、识别异常、正常的比例"""
def anays_detail(Station,Start_time,Stop_time,df_exe,str_temp,flag):
    str1 = df_exe['变电站']==Station
    str2 = df_exe['识别时间'].map(lambda x:Start_time <= x <= Stop_time)
    str3 = df_exe['结果分类']=='告警'
    str4 = df_exe['结果分类']=='识别异常'
    str5 = df_exe['结果分类']=='正常'
    r = []
    r.append(df_exe[str1 & str2 & str3].shape[0])
    r.append(df_exe[str1 & str2 & str4].shape[0])
    r.append(df_exe[str1 & str2 & str5].shape[0])
    total = df_exe[str1 & str2].shape[0]
    temp = ('%s从 %s 至 %s 期间,%s类型巡检结果中正常点位：%d，占比：%.3f%%；'
           '告警点位：%d，占比：%.3f%%；识别异常点位：%d，'
            '占比：%.3f%%；'%(Station,Start_time,Stop_time,str_temp,r[2],r[2]/total,r[1],r[1]/total,r[0],r[0]/total))
    plt.pie(x = [r[2]/total,r[1]/total,r[0]/total],labels = ['正常','识别异常','告警'])
    plt.title(Station+'巡检结果一览')
    print(temp)
    if flag == True:
        f1 = open('%s%s至%s%s巡检识别异常点位（至少出现过一次）.txt'%(Station,str_temp,Start_time,Stop_time),'a')
        f2 = open('%s%s至%s%s巡检告警点位（至少出现过一次.txt'%(Station,str_temp,Start_time,Stop_time),'a')
        for tp in df_exe[str1 & str2 & str4]['点位名称'].unique():
            f1.write(tp+'\n')
        for tp in df_exe[str1 & str2 & str3]['点位名称'].unique():
            f2.write(tp+'\n')
    else:
        pass
    